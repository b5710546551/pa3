package lab7;
/**
 * Enum for collect units and numbers to convert area
 * @author Chinatip Vichian
 *
 */
public enum Area implements Unit{
	SQUARE_METER("squareMeter",1.00),
	SQUARE_FOOT("squareFoot", 0.0929030400000000026612187291648),
	RAI("rai",1600);
	
	/** String of name */
	public final String name;
	/** double of value */
	public final double value;
	/** Constructor for Area enum */
	Area(String name, double value){
		this.name = name;
		this.value = value;
	}
	/**
	 * Method to convert value from one unit to another that was put in the parameter
	 * @return value that already been converted
	 */
	@Override
	public double getValue() {
		return this.value;
	}
	/**
	 * Get Value of Area
	 * @return value of Area
	 */
	@Override
	public double convertTo(Unit unit, double amount) {
		return amount*this.getValue()/unit.getValue();
	}

}
