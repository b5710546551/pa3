package lab7;

/**
 * UnitConverter help convert Unit of two value by putting value and unit of both of them
 * @author Chinatip Vichian
 *
 */
public class UnitConverter {
	/**
	 * Constructor of the UnitConverter
	 * @param amount is value that wanted to convert
	 * @param fromUnit is unit that first come
	 * @param toUnit is unit that wanted to be converted
	 * @return value that already converted
	 */
	public double convert (double amount, Unit fromUnit,Unit toUnit ){
		return fromUnit.convertTo(toUnit, amount);

	}
	/**
	 * Get Unit Array
	 * @return array of Unit
	 */
	public Unit[] getUnits(UnitType utype){
		if(utype == UnitType.LENGTH)
			return Length.values();
		else if (utype == UnitType.WEIGHT)
			return Weight.values();
		else if  (utype == UnitType.AREA)
			return Area.values();
		return Time.values();
	}

} 
