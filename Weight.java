package lab7;
/**
 * Enum for collect units and numbers to convert weight
 * @author Chinatip Vichian
 *
 */
public enum Weight implements Unit{
	POUND("pound", 0.45359229219689715656599790531079),
	KILOGRAM("kilogram",1),
	TON("ton", 1000 ),
	BAHT("baht", 0.014999999925000000374999998125);
	/** String of name */
	public final String name;
	/** double of value */
	public final double value;
	/** Constructor for Weight enum */
	Weight(String name, double value){
		this.value = value;
		this.name = name;
	}
	/**
	 * Method to convert value from one unit to another that was put in the parameter
	 * @return value that already been converted
	 */
	public double getValue() {
		return this.value;
	}
	/**
	 * Get Value of Weight
	 * @return value of Weight
	 */
	public double convertTo(Unit unit, double amount) {
		return amount*this.getValue()/unit.getValue();
	}

}
