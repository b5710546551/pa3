package lab7;
/**
 * Unit is interface class for Length
 * @author Chinatip Vichian
 *
 */
public interface Unit {
	/**
	 * Get value of Unit
	 * @return value of unit
	 */
	double getValue();
	/**
	 * Get string of unit
	 * @return name of the unit
	 */
	String toString();
	/**
	 * Convert value from one unit to another
	 * @param unit is unit that wanted to be converted
	 * @param amount is number of original that will be convert into another unit
	 * @return converted value in unit that was put in the parameter 
	 */
	double convertTo(Unit unit, double amount);
}
