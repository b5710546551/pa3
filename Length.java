package lab7;
/**
 * Enum for collect units and numbers to convert length
 * @author Chinatip Vichian
 *
 */
public enum Length implements Unit{
	METER("meter",1.00),
	CENTIMETER("centimeter",0.01),
	KILOMETER("kilometer",1000.0),
	MILE("mile",1609.344),
	FOOT("foot",0.30480),
	WA("wa",2.0);
	/** String of name */
	public final String name;
	/** double of value */
	private final double value;
	/** Constructor for Length enum */
	Length(String name, double value){
		this.name = name;
		this.value= value;
	}
	/**
	 * Method to convert value from one unit to another that was put in the parameter
	 * @return value that already been converted
	 */
	@Override
	public double convertTo(Unit unit, double amount) {
		return (amount*this.getValue())/unit.getValue();
	}
	/**
	 * Get Value of Length
	 * @return value of Length
	 */
	@Override
	public double getValue() {
		return this.value;
	}
	

}
