package lab7;


import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * UI for converting Lenght, Area and Weight in different units.
 * @author Chinatip Vichian
 *
 */
public class ConverterUI extends JFrame implements Runnable{
	private JButton converter,clear;
	private JLabel label ;
	private JComboBox<String> unit1,unit2;
	private UnitConverter unitconverter;
	private JTextField inputField1, inputField2;
	private int numSwitch =1;
	private Container container = new Container();
	private JPanel panel2;
	private UnitType utype;

	/**
	 * Constructor for ConverterUI
	 * @param uc is an UnitConverter
	 */
	public ConverterUI(UnitConverter uc){
		this.unitconverter = uc;
		this.setTitle("Simple Converter");
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		utype = UnitType.LENGTH;
		initComponents();
	}
	
	/** Call components in this UI */
	public void initComponents(){
		numSwitch =1;
		super.setContentPane(container);
		container.setLayout(new GridLayout(2, 1));
		JPanel panel1 = new JPanel();
		panel1.setLayout(new GridLayout());
		JMenuBar menuBar = new JMenuBar();
		JMenu menu = new JMenu();
		menu.setText("  Unit Type  ");

		JMenuItem item1 = new JMenuItem("   Length");
		item1.addActionListener(new lengthListener());
		JMenuItem item2 = new JMenuItem("   Area");
		item2.addActionListener(new areaListener());
		JMenuItem item3 = new JMenuItem("   Weight");
		item3.addActionListener(new weightListener());
		JMenuItem item4 = new JMenuItem("   Time");
		item4.addActionListener(new timeListener());
		JMenuItem exit = new JMenuItem("   Exit");
		exit.addActionListener(new ExitAction());

		menu.add(item1);
		menu.add(item2);
		menu.add(item3);
		menu.add(item4);
		menu.addSeparator();
		menu.add(exit);
		menuBar.add(menu);
		panel1.add(menuBar,LEFT_ALIGNMENT);

		container.add(panel1);
		panel2 = new JPanel();
		inputField1 = new JTextField(15);
		inputField2 = new JTextField(15);
		inputField1.setForeground(Color.BLACK);
		inputField2.setForeground(Color.BLACK);
		inputField1.addActionListener(new ConvertButtonListener());
		inputField2.addActionListener(new ConvertButtonListener());
		inputField1.addMouseListener(new SwitchTextField1());
		inputField2.addMouseListener(new SwitchTextField2());
		label = new JLabel(" = ");
		converter = new JButton("Convert");
		clear = new JButton("Clear");

		converter.addActionListener(new ConvertButtonListener());
		clear.addActionListener(new ClearButtonListener());
		unit1 = new JComboBox(unitconverter.getUnits(utype));
		unit2 = new JComboBox(unitconverter.getUnits(utype));
		panel2.add(inputField1);
		panel2.add(unit1);
		panel2.add(label );
		panel2.add(inputField2);
		panel2.add(unit2);
		panel2.add(converter);
		panel2.add(clear);
		panel2.setLayout(new FlowLayout());
		container.add(panel2);
		this.pack();
	}
	
	/** ActionListener for menu of Length */
	class lengthListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			utype = UnitType.LENGTH;
			container.removeAll();
			initComponents();
		}

	}
	/** ActionListener for menu of Area */
	class areaListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			utype = UnitType.AREA;
			container.removeAll();
			initComponents();
		}

	}
	/** ActionListener for menu of Weight */
	class weightListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			utype = UnitType.WEIGHT;
			container.removeAll();
			initComponents();
		}

	}
	/** ActionListener for menu of Time */
	class timeListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			utype = UnitType.TIME;
			container.removeAll();
			initComponents();
		}

	}
	/** ActionListener for Exit the UI */
	class ExitAction extends AbstractAction {
		public void actionPerformed(ActionEvent evt) {
			System.exit(0);
		}
	}
	/** ActionListener for inputField1 to make inputField2 unable */
	class SwitchTextField1 implements MouseListener{
		/** clicking inputField1 make inputField2 unable */
		@Override
		public void mouseClicked(MouseEvent e) {
			numSwitch=1;
			inputField1.setEnabled(true);
			inputField2.setEnabled(false);
			inputField1.setForeground(Color.BLACK);
			inputField2.setForeground(Color.BLACK);
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mouseExited(MouseEvent e) {
			// TODO Auto-generated method stub
		}

		@Override
		public void mousePressed(MouseEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mouseReleased(MouseEvent e) {
			// TODO Auto-generated method stub

		}

	}
	/** ActionListener for inputField2 to make inputField1 unable */
	class SwitchTextField2 implements MouseListener{
		/** clicking inputField2 make inputField1 unable */
		@Override
		public void mouseClicked(MouseEvent e) {
			numSwitch=2;
			inputField2.setEnabled(true);
			inputField1.setEnabled(false);
			inputField1.setForeground(Color.BLACK);
			inputField2.setForeground(Color.BLACK);
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mouseExited(MouseEvent e) {
			// TODO Auto-generated method stub
		}

		@Override
		public void mousePressed(MouseEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mouseReleased(MouseEvent e) {
			// TODO Auto-generated method stub

		}
	}
	/** ActionListener for Convert Button */
	class ConvertButtonListener implements ActionListener {
		public void actionPerformed( ActionEvent evt ) {
			String s = "";
			if(numSwitch==1){
				s = inputField1.getText().trim();
			}
			else{
				s = inputField2.getText().trim();
			}
			double value=0;
			try {
				value = Double.valueOf( s );
				inputField1.setForeground(Color.BLACK);
				inputField2.setForeground(Color.BLACK);
			} catch (Exception e) {
				if(numSwitch==1)
					inputField1.setForeground(Color.RED);
				else
					inputField2.setForeground(Color.RED);
			}

			Unit unit_1 = (Unit) utype.getUnit(unit1.getSelectedItem().toString().toUpperCase(),utype);
			Unit unit_2 = (Unit) utype.getUnit(unit2.getSelectedItem().toString().toUpperCase(),utype); 

			double result=0;
			if(numSwitch==1){
				result = unitconverter.convert( value, unit_1, unit_2);
				inputField2.setText( "" + result );
			}
			else{
				result = unitconverter.convert( value, unit_2, unit_1);
				inputField1.setText( "" + result );
			}
		}
	}
	/** ActionListener for Clear Button */
	class ClearButtonListener implements ActionListener {
		public void actionPerformed(ActionEvent evt){
			inputField1.setText("");
			inputField2.setText("");
			inputField1.setEnabled(true);
			inputField2.setEnabled(true);
			inputField1.setForeground(Color.BLACK);
			inputField2.setForeground(Color.BLACK);
		}
	}
	/**  Run the UI */
	public void run(){
		this.setVisible(true);
	}
	/** Main method */
	public static void main (String [] args){
		UnitConverter ui = new UnitConverter();
		ConverterUI convert = new ConverterUI(ui);
		convert.run();
	}
}