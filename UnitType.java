package lab7;
/**
 * Enum for collecting unit types
 * @author Chinatip Vichian
 *
 */
public enum UnitType {
	LENGTH,
	AREA,
	WEIGHT,
	TIME;
	/**  
	 * 
	 * @param name of selected unit from CnverterUI
	 * @param utype is UnitType that are converting.
	 * @return an unit from unit type that their name was choosed in converterUI.
	 */
	public Unit getUnit(String name,UnitType utype){
		if(utype == UnitType.LENGTH)
			return Length.valueOf(name);
		else if (utype == UnitType.WEIGHT)
			return Weight.valueOf(name);
		else if (utype == UnitType.TIME)  
			return Time.valueOf(name);
		return Area.valueOf(name);
	}
}
