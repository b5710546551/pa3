package lab7;
/**
 * Enum for collect units and numbers to convert time
 * @author Chinatip Vichian
 *
 */
public enum Time implements Unit{
	MINUTE("minute",1.00),
	SECOND("second", 0.01666666666666666666666),
	HOUR("hour",60),
	DAY("day",1440);
	
	/** String of name */
	public final String name;
	/** double of value */
	public final double value;
	/** Constructor for Time enum */
	Time(String name, double value){
		this.name = name;
		this.value = value;
	}
	/**
	 * Method to convert value from one unit to another that was put in the parameter
	 * @return value that already been converted
	 */
	@Override
	public double getValue() {
		return this.value;
	}
	/**
	 * Get Value of Time
	 * @return value of Time
	 */
	@Override
	public double convertTo(Unit unit, double amount) {
		return amount*this.getValue()/unit.getValue();
	}
}
